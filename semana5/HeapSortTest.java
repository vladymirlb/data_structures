/*
* Desenvolvido para a disciplina Laborat�rio de Estrutura de Dados
* Curso de Bacharelado em Ci�ncia da Computa��o
* Departamento de Sistemas e Computa��o
* Universidade Federal de Campina Grande
*
* Professor: Marco Aur�lio Spohn
* Monitor: Epitacio Miguel
*
*/

import junit.framework.*;

public class HeapSortTest extends TestCase{

private HeapSort a;
private int[] lista1 = {4};
private int[] lista2 = {55,55,55,55,55};
private int[] lista3 = {4,2,5,1,3};
private int[] lista4 = {10,8,6,4,2,1};
private int[] lista5 = {9,12,77,194,484,1000,3030,4777,10111};
private int[] lista6 = {4777,484,194,1000,77,3030,10111,9,12};
private int[] lista7 = {12,18,40,417,1998,3,73,19,46,199,47,1,19,11,2,2,1};
private int[] lista8 = {4,12,12,4,12,4,12,4,12,4,12};
private int[] lista9 = {-1,0,-19,8,6,4,2,1,-500,-120,50};
private int[] lista10 = {-1,1,-2,2,-3,3,-4,4,-5,5,-6,6,-7,7,-8,8,-9,9};

public HeapSortTest(String name) {
super(name);
}

public static void main(String[] args) {
junit.textui.TestRunner.run(suite());
}

protected void setUp() {
a = new HeapSort();
}

public static Test suite() {
return new TestSuite(HeapSortTest.class);
}

public void testHeapSort() {
a.HeapSortOrdenacao(lista1);
assertEquals("1","4",a.toString());
a.HeapSortOrdenacao(lista2);
assertEquals("2","55 55 55 55 55",a.toString());
a.HeapSortOrdenacao(lista3);
assertEquals("3","1 2 3 4 5",a.toString());
a.HeapSortOrdenacao(lista4);
assertEquals("4","1 2 4 6 8 10",a.toString());
a.HeapSortOrdenacao(lista5);
assertEquals("5","9 12 77 194 484 1000 3030 4777 10111",a.toString());
a.HeapSortOrdenacao(lista6);
assertEquals("6","9 12 77 194 484 1000 3030 4777 10111",a.toString());
a.HeapSortOrdenacao(lista7);
assertEquals("7","1 1 2 2 3 11 12 18 19 19 40 46 47 73 199 417 1998",a.toString());
a.HeapSortOrdenacao(lista8);
assertEquals("8","4 4 4 4 4 12 12 12 12 12 12",a.toString());
a.HeapSortOrdenacao(lista9);
assertEquals("9","-500 -120 -19 -1 0 1 2 4 6 8 50",a.toString());
a.HeapSortOrdenacao(lista10);
assertEquals("10","-9 -8 -7 -6 -5 -4 -3 -2 -1 1 2 3 4 5 6 7 8 9",a.toString());
}
}