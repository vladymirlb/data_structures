/**
 * Classe que implementa HeapSort
 * @author Vladymir
 *
 */

public class HeapSort {
	
	private int[] lista;
	
	public HeapSort(){}

	public void HeapSortOrdenacao(int[] lista1) {
		lista = heapSort(lista1);
	}
	
	public static int[] heapSort(int[] lista1){
		buildMaxHeap(lista1);
		int tamanho=lista1.length-1;
		for(int i=lista1.length-1;i>0;i--){		
			swap(lista1, i, 0);
			maxheapfy(lista1, 0,tamanho);
			tamanho--;
		}
		return lista1;
	}
	
	private static int left(int i){
		return (i*2)+1;
	}
	
	private static int right(int i){
		return (i*2)+2;
	}
	
	public static void maxheapfy(int[] array, int i, int tamanho){
		int l=left(i);
		int r=right(i);
		int largest;
		
		
		if(l<tamanho && array[l]>array[i])
			largest = l;
		else largest = i;
		
		if(r<tamanho && array[r]>array[largest])
			largest=r;
		
		if(largest!=i){
			swap(array,i,largest);
			maxheapfy(array, largest,tamanho);
		}
	}
	
	public static void buildMaxHeap(int[] array){
		int heapsize = array.length;
		for(int i=heapsize/2; i>=0 ;i--)
			maxheapfy(array, i,heapsize);
	}
	
	private static void swap(int[] array,int posi,int posf){
		int aux = array[posi];
		array[posi]=array[posf];
		array[posf]=aux;
	}
	
	public String toString(){
		String retorno = new String();
		for(int i=0;i<lista.length;i++)
			retorno+=String.valueOf(lista[i])+" ";
		retorno=retorno.trim();
		return retorno;
	}
	
}
