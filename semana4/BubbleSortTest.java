/*
* Desenvolvido para a disciplina Laboratório de Estrutura de Dados
* Curso de Bacharelado em Ciência da Computação
* Departamento de Sistemas e Computação
* Universidade Federal de Campina Grande
*
* Professor: Marco Aurélio Spohn
* Monitor: Epitacio Miguel
*
*/

import junit.framework.*;


public class BubbleSortTest extends TestCase{
		
		protected int[] A;
		protected int[] B;
		protected int[] C;
		protected int[] D;
		protected int[] E;
		protected int[] F;
		protected int[] G;
		protected int[] H;
		protected int[] I;
		protected int[] J;
	  
	  public BubbleSortTest(String name) {
		  super(name);
	  }

	  public static void main(String[] args) {
		  junit.textui.TestRunner.run(suite());
	  }
	  
	  protected void setUp() {
		  A = new int[] {4};
		  B = new int[] {55,55,55,55,55};
		  C = new int[] {4,2,5,1,3};
		  D = new int[] {10,8,6,4,2,1};
		  E = new int[] {9,12,77,194,484,1000,3030,4777,10111};
		  F = new int[] {4777,484,194,1000,77,3030,10111,9,12};
		  G = new int[] {12,18,40,417,1998,3,73,19,46,199,47,1,19,11,2,2,1};
		  H = new int[] {4,12,12,4,12,4,12,4,12,4,12};
		  I = new int[] {-1,0,-19,8,6,4,2,1,-500,-120,50};
		  J = new int[] {-1,1,-2,2,-3,3,-4,4,-5,5,-6,6,-7,7,-8,8,-9,9};
		  
	  }
	  
	  public static Test suite() {
		  return new TestSuite(BubbleSortTest.class);
	  }
	  
	  public void testBobbleSort() {
		  assertEquals("1","4",BubbleSort.bubbleSort(A));
		  assertEquals("2","55 55 55 55 55",BubbleSort.bubbleSort(B));		  
		  assertEquals("3","1 2 3 4 5",BubbleSort.bubbleSort(C));
		  assertEquals("4","1 2 4 6 8 10",BubbleSort.bubbleSort(D));
		  assertEquals("5","9 12 77 194 484 1000 3030 4777 10111",BubbleSort.bubbleSort(E));
		  assertEquals("6","9 12 77 194 484 1000 3030 4777 10111",BubbleSort.bubbleSort(F));
		  assertEquals("7","1 1 2 2 3 11 12 18 19 19 40 46 47 73 199 417 1998",BubbleSort.bubbleSort(G));
		  assertEquals("8","4 4 4 4 4 12 12 12 12 12 12",BubbleSort.bubbleSort(H));
		  assertEquals("9","-500 -120 -19 -1 0 1 2 4 6 8 50",BubbleSort.bubbleSort(I));
		  assertEquals("9","-9 -8 -7 -6 -5 -4 -3 -2 -1 1 2 3 4 5 6 7 8 9",BubbleSort.bubbleSort(J));		  
			  
	  }
}
