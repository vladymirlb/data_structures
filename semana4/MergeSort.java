
/**
 * Classe que faz ordena��o de vetores por MergeSort
 * @author Vladymir
 *
 */
public class MergeSort {

    private int[] lista;
    
    

    public MergeSort() {
	}


	public static int[] merge_Sort(int[] lista1) {
		int[] esquerda, direita, resultado;
		if(lista1.length%2==0){
			esquerda =  new int[lista1.length/2];
			direita = new int[lista1.length/2];
		}
		else{
			esquerda =  new int[lista1.length/2];
			direita = new int[lista1.length/2+1];
		}
		resultado = new int[lista1.length];
		
		if(lista1.length<=1)
			return lista1;
		else {
			int meio = lista1.length/2;
			int cont=0;
			for(int i=0;i<meio;i++){
				esquerda[i]=lista1[i];
			}
			for(int i=meio;i<lista1.length;i++){
				direita[cont]=lista1[i];
				cont++;
			}
		}
		
		esquerda = merge_Sort(esquerda);
		direita = merge_Sort(direita);
		resultado = merge(esquerda, direita);
		return resultado;
	}




	private static int[] merge(int[] esquerda, int[] direita) {
		int[] resultado = new int[esquerda.length+direita.length];
		int lValue = esquerda.length;
		int rValue = direita.length;
		int econtador, dcontador,contador;
		econtador=dcontador=contador=0;
	    while (lValue > 0 && rValue > 0){
	        if (esquerda[econtador] <= direita[dcontador]){
	            resultado[contador] = esquerda[econtador];
	            lValue--;
	            econtador++;
	        }
	        else{
	            resultado[contador] = direita[dcontador];
	            rValue--;
	            dcontador++;
	        }
	        contador++;
	        
	        
	    }
	    if (lValue > 0){
	    	for(int i=econtador;i<esquerda.length;i++){
	    		resultado[contador]=esquerda[i];
	    		contador++;
	    	}
	    	
	    }
	    if (rValue > 0){
	    	for(int i=dcontador;i<direita.length;i++){
	    		resultado[contador]=direita[i];
	    		contador++;
	    	}
	    	
	    }
	    
	    return resultado;
		
	}




	public void mergeSort(int[] lista1) {
		this.lista = merge_Sort(lista1);
	}
	
	public String toString(){
		String retorno = new String();
		for(int i=0;i<lista.length;i++)
			retorno+=String.valueOf(lista[i])+" ";
		retorno = retorno.trim();
		return retorno;
	}
}
