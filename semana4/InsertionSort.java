/**
 * Classe que faz ordena��o de vetores por InsertionSort
 * @author Vladymir.
 *
 */
public class InsertionSort {

	public InsertionSort(){
	}

	public static String insertSort(int[] a) {
		String retorno = new String();
		for(int i=1;i<a.length;i++){
			int aux=a[i];
			int j = i;
			while((j > 0) && (aux < a[j-1])){
				a[j] = a[j-1];
				j--;
			}
			a[j] = aux;
		}
		
		for(int i=0;i<a.length;i++)
			retorno+=String.valueOf(a[i])+" ";
		
		retorno=retorno.trim();
		return retorno;
	}
	
	
}
