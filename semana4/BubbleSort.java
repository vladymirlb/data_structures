/**
 * Classe que faz ordena��o por BubbleSort
 * @author Vladymir.
 *
 */
public class BubbleSort {

	private static boolean troca;
	
	public BubbleSort(){
	}

	public static String bubbleSort(int[] a) {
		String retorno = new String();
		troca = true;
		while(troca){
			troca = false;
			for(int i=0;i<a.length-1;i++){
				if(a[i]>a[i+1]) {
					int aux = a[i];
					a[i] = a[i+1];
					a[i+1] = aux;
					troca = true;
				}
			}
		}
		
		for(int i=0;i<a.length;i++)
			retorno+=String.valueOf(a[i])+" ";
		
		retorno=retorno.substring(0, retorno.length()-1);
		return retorno;
	}
}
