/*
* Desenvolvido para a disciplina Laborat�rio de Estrutura de Dados
* Curso de Bacharelado em Ci�ncia da Computa��o
* Departamento de Sistemas e Computa��o
* Universidade Federal de Campina Grande
*
* Professor: Marco Aur�lio Spohn
* Monitor: Epitacio Miguel
*
*/

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class HuffmanCodeTest extends TestCase{

protected Huffman lista1,lista2,lista3,lista4,lista5,lista6,lista7,lista8,lista9,
lista10,lista11,lista12,lista13,lista14,lista15,lista16,lista17,lista18,lista19,
lista20,lista21,lista22,lista23,lista24,lista25,lista26,lista27,lista28,lista29,
lista30,lista31,lista32,lista33,lista34,lista35,lista36,lista37,lista38,lista39,
lista40,lista41,lista42,lista43,lista44,lista45,lista46,lista47,lista48,lista49,lista50;

public static Test suite() {
return new TestSuite(HuffmanCodeTest.class);
}

protected void setUp() {

lista1 = new Huffman("AAFDECbaDaFcAFFAeC");

lista2 = new Huffman("eEAcCafceccDEEfFEDa");

lista3 = new Huffman("FAdeCBaCcAbACdFc");

lista4 = new Huffman("ABfAAbfAc");

lista5 = new Huffman("ECBebee");

lista6 = new Huffman("acDbBA");

lista7 = new Huffman("eaABfCFFCfcfeCACd");

lista8 = new Huffman("eaDaCFaeEDfc");

lista9 = new Huffman("cAbadCdADcEaacD");

lista10 = new Huffman("bbbEeecCDaEDbDFbdBdE");

lista11 = new Huffman("DfACDDaCDfDf");

lista12 = new Huffman("BcDEdee");

lista13 = new Huffman("BfddFDff");

lista14 = new Huffman("AFDaAcEbbCbDeeEaaDBa");

lista15 = new Huffman("DbCae");

lista16 = new Huffman("ECdbBBed");

lista17 = new Huffman("BBdAcCDfEffDdadcFcCb");

lista18 = new Huffman("bCFDABCaDCcFBbBae");

lista19 = new Huffman("EEBcfb");

lista20 = new Huffman("aedcEeAF");

lista21 = new Huffman("adAFCeCCDbDdFEECE");

lista22 = new Huffman("ccfDcaFCEefFBBfDF");

lista23 = new Huffman("dbEEeDABcCBFBDBFb");

lista24 = new Huffman("CECdDeFDEcAffE");

lista25 = new Huffman("bdFFAbE");

lista26 = new Huffman("ACfbBF");

lista27 = new Huffman("fCfeCCdeaBfB");

lista28 = new Huffman("dfFDbeDBDEebBecddDEd");

lista29 = new Huffman("abFafdccCfEcbb");

lista30 = new Huffman("bFFbdeDAaAebeFcaFfa");

lista31 = new Huffman("bABabaEC");

lista32 = new Huffman("BfdAcCDfFCdddDEcadD");

lista33 = new Huffman("ADEFdB");

lista34 = new Huffman("FFAEaAAbbCBDfbEEb");

lista35 = new Huffman("BedeAAdE");

lista36 = new Huffman("eDDadDAFeAdEf");

lista37 = new Huffman("babCbeffFADe");

lista38 = new Huffman("FCacc");

lista39 = new Huffman("EEDcBBfDbFcEb");

lista40 = new Huffman("acfaCafaEbBAAbeaEF");

lista41 = new Huffman("eCecaACfBAFbaBa");

lista42 = new Huffman("FfFAfaCCAaDaFbbFbbD");

lista43 = new Huffman("FDDdEbeaFFFaBCEd");

lista44 = new Huffman("bEAcBdFfcfdc");

lista45 = new Huffman("cEdBeAdDfaaCAEabDf");

lista46 = new Huffman("edaCecfCEA");

lista47 = new Huffman("aecEBfdFEbAdBCaB");

lista48 = new Huffman("dfcFdFCcffAfE");

lista49 = new Huffman("DDddbCaAee");

lista50 = new Huffman("eBbAbcdceFDBDcADE");
}

public void testHuffman(){
assertEquals("1","A 11 B 000 C 101 D 001 E 100 F 01 ",lista1.toString());
assertEquals("A 101 C 01 D 100 E 11 F 00 ",lista2.toString());
assertEquals("A 01 B 001 C 11 D 100 E 000 F 101 ",lista3.toString());
assertEquals("A 0 B 111 C 110 F 10 ",lista4.toString());
assertEquals("B 01 C 00 E 1 ",lista5.toString());
assertEquals("A 10 B 11 C 00 D 01 ",lista6.toString());
assertEquals("A 00 B 0110 C 10 D 0111 E 010 F 11 ",lista7.toString());
assertEquals("A 01 C 110 D 111 E 10 F 00 ",lista8.toString());
assertEquals("A 11 B 000 C 01 D 10 E 001 ",lista9.toString());
assertEquals("A 0010 B 11 C 000 D 01 E 10 F 0011 ",lista10.toString());
assertEquals("A 110 C 111 D 0 F 10 ",lista11.toString());
assertEquals("B 110 C 111 D 10 E 0 ",lista12.toString());
assertEquals("B 10 D 11 F 0 ",lista13.toString());
assertEquals("A 10 B 00 C 1111 D 110 E 01 F 1110 ",lista14.toString());
assertEquals("A 110 B 111 C 00 D 01 E 10 ",lista15.toString());
assertEquals("B 11 C 00 D 01 E 10 ",lista16.toString());
assertEquals("A 1111 B 110 C 01 D 10 E 1110 F 00 ",lista17.toString());
assertEquals("A 111 B 10 C 01 D 001 E 000 F 110 ",lista18.toString());
assertEquals("B 10 C 00 E 11 F 01 ",lista19.toString());
assertEquals("A 01 C 100 D 101 E 11 F 00 ",lista20.toString());
assertEquals("A 1111 B 1110 C 00 D 01 E 10 F 110 ",lista21.toString());
assertEquals("A 000 B 001 C 01 D 100 E 101 F 11 ",lista22.toString());
assertEquals("A 010 B 11 C 011 D 101 E 00 F 100 ",lista23.toString());
assertEquals("A 110 C 111 D 00 E 10 F 01 ",lista24.toString());
assertEquals("A 110 B 01 D 111 E 00 F 10 ",lista25.toString());
assertEquals("A 00 B 10 C 01 F 11 ",lista26.toString());
assertEquals("A 000 B 110 C 01 D 001 E 111 F 10 ",lista27.toString());
assertEquals("B 111 C 1100 D 0 E 10 F 1101 ",lista28.toString());
assertEquals("A 110 B 00 C 10 D 1110 E 1111 F 01 ",lista29.toString());
assertEquals("A 01 B 110 C 000 D 001 E 111 F 10 ",lista30.toString());
assertEquals("A 11 B 0 C 100 E 101 ",lista31.toString());
assertEquals("A 100 B 1010 C 111 D 0 E 1011 F 110 ",lista32.toString());
assertEquals("A 110 B 111 D 10 E 00 F 01 ",lista33.toString());
assertEquals("A 01 B 10 C 1100 D 1101 E 111 F 00 ",lista34.toString());
assertEquals("A 01 B 00 D 10 E 11 ",lista35.toString());
assertEquals("A 01 D 11 E 10 F 00 ",lista36.toString());
assertEquals("A 110 B 01 C 000 D 001 E 111 F 10 ",lista37.toString());
assertEquals("A 00 C 1 F 01 ",lista38.toString());
assertEquals("B 10 C 110 D 111 E 01 F 00 ",lista39.toString());
assertEquals("A 0 B 101 C 100 E 110 F 111 ",lista40.toString());
assertEquals("A 11 B 00 C 01 E 100 F 101 ",lista41.toString());
assertEquals("A 10 B 00 C 010 D 011 F 11 ",lista42.toString());
assertEquals("A 001 B 110 C 000 D 01 E 111 F 10 ",lista43.toString());
assertEquals("A 000 B 110 C 01 D 111 E 001 F 10 ",lista44.toString());
assertEquals("A 10 B 010 C 011 D 00 E 111 F 110 ",lista45.toString());
assertEquals("A 00 C 10 D 010 E 11 F 011 ",lista46.toString());
assertEquals("A 111 B 01 C 100 D 101 E 00 F 110 ",lista47.toString());
assertEquals("A 1110 C 10 D 110 E 1111 F 0 ",lista48.toString());
assertEquals("A 00 B 100 C 101 D 11 E 01 ",lista49.toString());
assertEquals("A 001 B 01 C 110 D 10 E 111 F 000 ",lista50.toString());
}
}