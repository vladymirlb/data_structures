/**
 * Nodo da arvore
 * @author Vladymir
 *
 */
public class Nodo implements Comparable{

	private String info;
	private int frequencia;
	String codigo;
	protected Nodo esquerda;
	protected Nodo direita;
	protected Nodo pai;
	
	public Nodo(String info){
		codigo = new String();
		this.info = info;
		esquerda = null;
		direita = null;
		pai = null;
		frequencia = 0;
	}
	
	public Nodo(String info, int freq){
		codigo = new String();
		this.info = info;
		esquerda = null;
		direita = null;
		pai = null;
		frequencia=freq;
	}
	
	public int getFreq(){
		return frequencia;
	}
	
	public String getInfo(){
		return info;
	}
	
	public void setNodoEsquerda(Nodo nodo){
		esquerda = nodo;
	}
	
	public void setNodoDireita(Nodo nodo){
		direita = nodo;
	}
	
	public void setPai(Nodo nodo){
		pai = nodo;
	}
	
	public Nodo getPai(){
		return pai;
	}
	
	public Nodo getNodoEsquerda(){
		return esquerda;
	}
	
	public Nodo getNodoDireita(){
		return direita;
	}
	
	public String toString(){
		return info+" "+frequencia;
	}


	public void setInfo(String info2) {
		this.info = info2;
	}
	
	public void setCodigo(String valor){
		codigo+=valor;
	}
	
	public String getCodigo(){
		return codigo;
	}

	@Override
	public int compareTo(Object o){
		if(!(o instanceof Nodo)){
			return 0;
		}
		
		Nodo nodo = (Nodo) o;
		
		return this.info.charAt(0)-nodo.getInfo().charAt(0);
		
		
	}
}
