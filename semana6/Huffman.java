/**
 * Classe Huffman Code.
 * @author Vladymir Bezerra 20711437
 */

import java.util.ArrayList;
import java.util.Collections;


public class Huffman {
	
	private PriorityQueue fila;
	protected Nodo raiz;
	private String inOrder;
	private String codigo = new String();
	private ArrayList<Nodo> Lista = new ArrayList<Nodo>();

	public Huffman(String string) {
		fila = new PriorityQueue(string);
		raiz = huffmanCode(this.fila);
		inOrder = new String();
	}

	public Nodo huffmanCode(PriorityQueue fila){
		int tamanho = fila.getSize();
		PriorityQueue huffman = fila;
		for(int i=0;i<tamanho-1;i++){
			Nodo z, esquerda, direita;
			esquerda = huffman.extractMin(huffman.fila);
			direita = huffman.extractMin(huffman.fila);
			z = new Nodo("",esquerda.getFreq()+direita.getFreq());
			z.setNodoEsquerda(esquerda);
			z.setNodoDireita(direita);
			huffman.insereNodo(z);
		}
		return huffman.extractMin(huffman.fila);
	}
	
	public Nodo getRaiz(){
		return raiz;
	}
	
	
	public String toString() {
		String retorno = new String();
		gerarCodigos(raiz, "");
		Collections.sort(Lista);
		for(int i=0;i<Lista.size();i++)
			retorno+=Lista.get(i).getInfo()+" "+Lista.get(i).getCodigo()+" ";
		return retorno;
	}
	
	public void gerarCodigos(Nodo nodo, String codigo){
		this.codigo+=codigo;
		if(nodo.esquerda == null && nodo.direita == null){
			nodo.setCodigo(codigo);
			Lista.add(nodo);

		}
		else {
			gerarCodigos(nodo.esquerda, codigo+"0");
			gerarCodigos(nodo.direita,codigo+"1");
		}
	}
}
