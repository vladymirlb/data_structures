/*

* Desenvolvido para a disciplina Laborat�rio de Estrutura de Dados

* Curso de Bacharelado em Ci�ncia da Computa��o

* Departamento de Sistemas e Computa��o

* Universidade Federal de Campina Grande

*

* Professor: Marco Aur�lio Spohn

* Monitor: Epitacio Miguel

*

*/


import junit.framework.*;


public class BSTTest extends TestCase{


protected BST a;

protected BST b;

protected BST c;

protected BST d;

protected BST e;

protected BST f;


public BSTTest(String name) {

super(name);

}


public static void main(String[] args) {

junit.textui.TestRunner.run(suite());

}


protected void setUp(){

a = new BST("10");

b = new BST("10 9 8 7 6 5 4 3 2 1");

c = new BST("10 15 2 3 4 5");

d = new BST("100 50 90 60 70 65 68 66");

e = new BST("100 125 10 25 50 36 54 87 12 5 6 7 130 4 33");

f = new BST("50 40 60 23 32 55 75 80 90 100 33 4 15 25 37");

}


public static Test suite() {

return new TestSuite(BSTTest.class);

}


public void testBSTInsercao() {

assertEquals("1","10",a.toStringInOrder());

assertEquals("2","1 2 3 4 5 6 7 8 9 10",b.toStringInOrder());

assertEquals("3","2 3 4 5 10 15",c.toStringInOrder());

assertEquals("4","50 60 65 66 68 70 90 100",d.toStringInOrder());

assertEquals("5","4 5 6 7 10 12 25 33 36 50 54 87 100 125 130",e.toStringInOrder());

assertEquals("6","4 15 23 25 32 33 37 40 50 55 60 75 80 90 100",f.toStringInOrder());

}


public void testBSTRemocao() {

a.removeNumeros("10");

assertEquals("1","",a.toStringPreOrder());


b.removeNumeros("10 8 6 2 1");

assertEquals("2","9 7 5 4 3",b.toStringPreOrder());


c.removeNumeros("2 10 15");

assertEquals("3","3 4 5",c.toStringPreOrder());


d.removeNumeros("50 60 65 66");

assertEquals("4","100 90 70 68",d.toStringPreOrder());


e.removeNumeros("33 100 25 54 5 12 50");

assertEquals("5","125 10 6 4 7 36 87 130",e.toStringPreOrder());


f.removeNumeros("4 15 23 25 50 55 60 75 80 90 100");

assertEquals("6","40 32 33 37",f.toStringPreOrder());

}

}