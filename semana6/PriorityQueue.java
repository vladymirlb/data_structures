/**
 * Classe Fila de Prioridades, usa Array como fila.
 * @author Vladymir Bezerra
 *
 */
public class PriorityQueue {

	private String info;
	private int tamanho = 0;
	
	private int[] frequencia= new int[26];
	private String[] caracteres = new String[26];
	private Nodo[] nodos = new Nodo[26];
	protected Nodo[] fila;
	
	public PriorityQueue(String string){
		tamanho = 0;
		this.info = string.toUpperCase();
		frequencia = contarFrequencia(info);
		nodos = gerarNodos(frequencia, caracteres);
		fila = new Nodo[tamanho];
		fila = insere(nodos);
	}

	public Nodo[] gerarNodos(int[] frequencia, String[] caracteres){
		Nodo[] nodos = new Nodo[26];
		for(int i=0;i<frequencia.length;i++){
			if(frequencia[i]!=0){
				nodos[i] = new Nodo(caracteres[i],frequencia[i]);
				tamanho++;
			}
		}
		return nodos;
	}
	public void insereNodo(Nodo nodo){
		for(int i=0;i<this.fila.length;i++){
			if(fila[i]==null){
				fila[i]=nodo;
				break;
			}
		}
		ordenaFila(fila);
	}
	public int getSize(){
		return tamanho;
	}
	public Nodo[] insere(Nodo[] nodos){
		Nodo aux = null;
		Nodo[] fila = new Nodo[tamanho];
		int a = 0;
		
		for(int i=0;i<nodos.length;i++){
			if(nodos[i]!=null){
				fila[a]=nodos[i];
				a++;
			}
		}
		ordenaFila(fila);
		return fila;
	}
	
	public void ordenaFila(Nodo[] nodos){
		String retorno = new String();
		for(int i=1;i<nodos.length;i++){
			Nodo aux=nodos[i];
			int j = i;
			while(aux!=null && (j > 0) && (aux.getFreq() < nodos[j-1].getFreq())){
				nodos[j] = nodos[j-1];
				j--;
			}
			nodos[j] = aux;
		}
	}
	
	public int[] contarFrequencia(String valores) {
		int contador[] = new int[frequencia.length];
		String letra = new String();
		for(int i=0;i<valores.length();i++){
			letra = valores.valueOf(valores.charAt(i));
			contador[valores.charAt(i)-'A']++;
			caracteres[valores.charAt(i)-'A']=letra;
		}
		
		return contador;
	}
	
	public String toStringFreq(){
		String retorno = new String();
		char a;
		for(int i=0;i<frequencia.length;i++){
			if(frequencia[i]!=0){
				retorno+=caracteres[i]+": "+String.valueOf(frequencia[i])+" ";
			}
		}
		return retorno.trim();
	}
	
	public String toString(){
		String retorno = new String();
		for(int i=0;i<fila.length;i++){
			if(fila[i]!=null)
				retorno+=fila[i].getInfo()+": "+String.valueOf(fila[i].getFreq())+"\n";
		}
		return retorno;
	}
	
	public Nodo extractMin(Nodo[] nodos){
		Nodo aux = null;
		Nodo temp = null;
		int i;
		if(nodos[0]!=null){
			aux = nodos[0];
			nodos[0] = null;
			for(i=1;i<nodos.length;i++){
				if(nodos[i]!=null)
					nodos[i-1]=nodos[i];
				else 
					break;
			}
			nodos[i-1]=null;
			tamanho--;
		}
		else aux = null;
		return aux;
	}
	
	
}
