/**
 * Binary Search Tree
 * @author Vladymir
 *
 */
public class BST {
	
	private String info;
	protected Nodo raiz;
	private String inOrder = new String();
	private String preOrder = new String();
	
	public BST(String string) {
		this.info = string;
		raiz = null;
		insereNaBST(string);
	}
	
	public BST(){
		raiz = null;
	}
	// INSERE NA BST
	public void insereNaBST(String string){
		String[] valores = string.split(" ");
		for(int i=0;i<valores.length;i++){
			insere(new Nodo(valores[i]));
		}
	}
	// INSERE
	public void insere(Nodo nodo){
		Nodo x, y, novo;
		y = null;
		x = raiz;
		
		while(x!=null){
			y=x;
			if(Integer.parseInt(nodo.getInfo())==Integer.parseInt(x.getInfo()))
					return;
			else if(Integer.parseInt(nodo.getInfo())<Integer.parseInt(x.getInfo()))
					x = x.esquerda;
			else
					x = x.direita;
		}
		
		novo = new Nodo(nodo.getInfo());
		novo.setPai(y);
		novo.setNodoDireita(null);
		novo.setNodoEsquerda(null);
		
		if (y == null)
			raiz = novo;
		else if(Integer.parseInt(nodo.getInfo())<Integer.parseInt(y.getInfo()))
			y.esquerda = novo;
		else
			y.direita = novo;
				
	}

	// TO STRING EM ORDEM
	public String toStringInOrder() {
		this.inOrder = new String();
		emOrdem(raiz);
		return this.inOrder.trim();
	}
	
	// PRE ORDEM
	public void preOrdem(Nodo nodo){
		if(nodo != null){
			this.preOrder+=nodo.getInfo()+" ";
			preOrdem(nodo.esquerda);
			preOrdem(nodo.direita);
		}
	}
	// EM ORDEM
	public void emOrdem(Nodo nodo){
		if(nodo != null){
			emOrdem(nodo.esquerda);
			this.inOrder+=nodo.getInfo()+" ";
			emOrdem(nodo.direita);
		}
	}

	public void removeNumeros(String string) {
		String[] valores = string.split(" ");
		
		for(int i=0;i<valores.length;i++){
			Nodo nodo = getNodo(new Nodo(valores[i]));
			removeNodo(nodo);
		}// TODO Auto-generated method stub
		
	}

	public String toStringPreOrder() {
		this.preOrder = new String();
		preOrdem(raiz);
		return this.preOrder.trim();
	}

	
	public Nodo treeMinimum(Nodo nodo){
		Nodo novo = nodo;
		while(novo.esquerda!=null){
			novo = novo.esquerda;
		}
		return novo;
	}
	
	public Nodo treeSuccessor(Nodo nodo){
		if(nodo.direita!=null)
			return treeMinimum(nodo.direita);
		Nodo y = nodo.getPai();
		while(y!=null && nodo.getInfo() == y.direita.getInfo()){
			nodo = y;
			y.setPai(y);
		}
		return y;
	}
	
	public void removeNodo(Nodo nodo){
		Nodo y,x;
		if(nodo.esquerda == null || nodo.direita == null)
			y = nodo;
		else
			y = treeSuccessor(nodo);
		if(y.esquerda!=null)
			x = y.esquerda;
		else
			x = y.direita;
		if(x!=null)
			x.setPai(y.getPai());
		if(y.getPai() ==null)
			raiz = x;
		else if(y == y.getPai().esquerda)
			y.getPai().esquerda = x;
		else
			y.getPai().direita = x;
		if(y.getInfo()!=nodo.getInfo()){
			System.out.println(y.getInfo());
			nodo.setInfo(y.getInfo());
		}
	}
	
	public Nodo getNodo(Nodo nodo){
		    if (raiz == null)
		        return null;

		    Nodo node = raiz;
		    int compareResult;
		    while ((compareResult = Integer.parseInt(node.getInfo()) - Integer.parseInt(nodo.getInfo())) != 0) {
		        if (compareResult > 0) {
		            if (node.esquerda != null)
		                node = node.esquerda;
		            else
		                return null;
		        } else {
		            if (node.direita != null)
		                node = node.direita;
		            else
		                return null;
		        }
		    }
		    return node;
		
	}

}
