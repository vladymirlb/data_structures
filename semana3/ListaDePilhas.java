/**
 * Esta classe representa uma lista de pilhas com tamanho pre-definido.
 * @author Vladymir
 *
 */
public class ListaDePilhas {
	
	private Pilha[] pilhas = new Pilha[26];

	private int limite_max;
	
	public ListaDePilhas(int limite, String frase){
		this.limite_max = limite;
		insere(frase);
	}
	/**
	 * Inicializa todas as pilhas.
	 */
	public void inicializa(){
		for(int i=0;i<pilhas.length;i++)
			pilhas[i] = new Pilha();
	}
	/**
	 * Insere em ordem alfabetica.
	 * @param frase String
	 */
	public void insere(String frase){
		String[] palavras = frase.split(" ");
		inicializa();
		for(int i=0;i<palavras.length;i++){
			int posicao = palavras[i].charAt(0) - 'A';
			if(pilhas[posicao].sizeOf() == limite_max)
				continue;
			else {
				pilhas[posicao].push(new Nodo(palavras[i]));
			}
		}
	}
	
	public String toString(){
		String retorno = new String();
		for(int i=0;i<pilhas.length;i++){
			if(pilhas[i].toString() != "")
				retorno+=pilhas[i].toString();
		}
		retorno = retorno.substring(0, retorno.length()-1);
		return retorno;
	}
}
