/**
 * Classe que representa uma Pilha. Pode-se adicionar(push) ou remover(pop) elementos da pilha.
 * LIFO.
 * @author Vladymir
 *
 */
public class Pilha {

	private Nodo topo;
	private int size;
	/**
	 * Construtor de pilha vazia.
	 */
	public Pilha(){
		topo = null;
		size = 0;
	}
	
	/**
	 * Adiciona um elemento na pilha.
	 * @param nodo Nodo que vai ser adicionado.
	 */
	public void push(Nodo nodo){
		if(topo == null)
			topo = nodo;
		else {
			nodo.setProximo(topo);
			topo = nodo;
		}
		size++;
	}
	/**
	 * Retira um elemento da pilha.
	 * @return Retorna o elemento removido.
	 */
	public Nodo pop() {
		Nodo aux = topo;			
		
		if(isEmpty()) return null;
		
		else if(topo.getProximo() == null)
			topo = null;
		else{
			topo = topo.getProximo();
			aux.setProximo(null);
		}
		return aux;
	}
	/**
	 * Teste se a pilha � vazia.
	 */
	public boolean isEmpty(){
		if(topo == null)
			return true;
		return false;
	}
	/**
	 * Verifica o tamanho da pilha.
	 * @return Um inteiro com o tamanho da pilha.
	 */
	public int sizeOf(){
		return size;
	}
	/**
	 * Imprime todos os elementos na ordem LIFO.
	 */
	public String toString(){
		String retorno = new String();
		Nodo aux = topo;
		while(aux != null){
			retorno+=aux.getInfo()+" ";
			aux = aux.getProximo();
		}
		return retorno;
	}
}
