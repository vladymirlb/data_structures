/**
 * Esta classe testa se a sequencia de Push e Pops eh valida.
 * @author Vladymir
 *
 */
public class ValidarSequencia {
	
	public ValidarSequencia(){
	}
	/**
	 * Testa se a sequencia eh correta ou nao.
	 * @param sequencia Sequencia de caracteres X ou S
	 * @return "Verdadeiro" se a sequencia for valida, "Falso" caso contrario.
	 */
	public String validarSequencia(String sequencia){
		int pilha=0;
		for(int i=0;i<sequencia.length();i++){
			if(sequencia.charAt(i)=='S')
				pilha++;
			else
				pilha--;
			
			if(pilha<0) return "Falso";
		}
		return "Verdadeiro";
	}
}
