import junit.framework.*;


public class ListaDePilhasTest extends TestCase{


public void testlistaDePilhas() {

assertEquals("1","A A A Bucket Distribuição De Entrada Em Funciona " +

"Gerada Linear Partir Quando Sort Tempo Uniforme Uma",new ListaDePilhas(3,"A Bucket Sort Funciona Em Tempo Linear Quando A Entrada" +

" Gerada A Partir De Uma Distribuição Uniforme").toString());

assertEquals("2","A Bucket De Em Funciona Gerada Linear Partir Quando Sort Tempo Uma", new ListaDePilhas(1,"A Bucket Sort " +

"Funciona Em Tempo Linear Quando A Entrada Gerada A Partir De Uma Distribuição Uniforme").toString());

assertEquals("3","A A Bucket Contagem Como Entrada Ordenação Porque Por Rápida Sobre Sort", new ListaDePilhas(2,"Como A " +

"Ordenação Por Contagem A Bucket Sort Rápida Porque Pressupõe Algo Sobre A Entrada").toString());

assertEquals("4","A Bucket Contagem Distribui Enquanto Gerada Inteiros Ordenação Por Que Sort Um", new ListaDePilhas(1,"Enquanto " +

"A Ordenação Por Contagem Presume Que A Entrada Consiste Em Inteiros Em Um Intervalo Pequeno Bucket Sort Presume Que A Entrada" +

" Gerada Por Um Processo Aleatório Que Distribui Elementos Uniformemente").toString());

assertEquals("5","A Baldes Bucket Dividir De E Em Intervalo Idéia N N Ou O Subintervalos Sort Tamanho",new ListaDePilhas(2,"A Idéia De " +

"Bucket Sort Dividir O Intervalo Em N Subintervalos De Igual Tamanho Ou Baldes E Depois Distribuir Os N Números De Entrada Entre Os Baldes").toString());

assertEquals("6","As Balde Cada Caiam Distribuídas Em Esperamos Entradas Em Muitos Números Não Que Que Sobre São Tendo Uniformemente Vista", new ListaDePilhas(4,"Tendo Em " +

"Vista Que As Entradas São Uniformemente Distribuídas Sobre Não Esperamos Que Muitos Números Caiam Em Cada Balde").toString());

assertEquals("7","A Baldes Balde Contidos Cada Depois E Em Listando Números Os Ordenamos Produzir Para Simplesmente Saída Um", new ListaDePilhas(2,"Para Produzir A " +

"Saída Simplesmente Ordenamos Os Números Em Cada Balde E Depois Percorremos Os Baldes Em Ordem Listando Os Elementos Contidos Em Cada Um").toString());

assertEquals("8","A Arranjo A Arranjo A Bucket Cada Código De ElementoA[i] E Elementos Entrada IA[i] No N Nosso O Pressupõe Para Que Que " +

"Satisfaz Sort Um", new ListaDePilhas(10,"Nosso Código Para Bucket Sort Pressupõe Que A Entrada Um " +

"Arranjo De N Elementos A E Que Cada ElementoA[i] No Arranjo Satisfaz A O IA[i]").toString());

assertEquals("9","Auxiliar Arranjo Código De E Exige Ligadas Listas Manter Mecanismo O Para Pressupõe Que Tais Um Um", new ListaDePilhas(2,"O Código Exige Um Arranjo Auxiliar De Listas Ligadas E " +

"Pressupõe Que Existe Um Mecanismo Para Manter Tais Listas").toString());

assertEquals("10","Bila DEDA Pasta Sola Tul Yu Zo", new ListaDePilhas(1,"Zo Zi Ze Yu Ye Ya Tul Tum Sola Sela Sala Bila Bola Bala DEDA " +

"DADA DODA Pasta Peste Pixe").toString());

assertEquals("11","Bola Bila DADA DEDA Peste Pasta Sela Sola Tum Tul Ye Yu Zi Zo", new ListaDePilhas(2,"Zo Zi Ze Yu Ye Ya Tul Tum Sola Sela Sala Bila Bola Bala DEDA DADA " +

"DODA Pasta Peste Pixe").toString());

assertEquals("12","Bala Bola Bila DODA DADA DEDA Pixe Peste Pasta Sala Sela Sola Tum Tul Ya " +

"Ye Yu Ze Zi Zo", new ListaDePilhas(3,"Zo Zi Ze Yu Ye Ya Tul Tum Sola Sela Sala Bila Bola Bala DEDA DADA " +

"DODA Pasta Peste Pixe").toString());

}

}


