/**
 * Classe nodo, representa um N� de uma Lista(filas, pilhas, etc.).
 * @author Vladymir
 *
 */
public class Nodo {
	private String info;
	private Nodo prox;
	
	/**
	 * Cria um nodo que contem a String info
	 */
	public Nodo(String info){
		this.info = info;
		this.prox = null;
	}
	
	/**
	 * Retorna a informa��o contida no nodo
	 * @return String info
	 */
	public String getInfo(){
		return info;
	}
	
	/**
	 * Retorna o proximo elemento
	 * @return Proximo nodo
	 */
	public Nodo getProximo(){
		return prox;
	}
	
	/**
	 * Seta o proximo nodo 
	 * @param nodo Proximo nodo da lista
	 */
	public void setProximo(Nodo nodo){
		this.prox = nodo;
	}
}