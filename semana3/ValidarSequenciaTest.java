import junit.framework.TestCase;


public class ValidarSequenciaTest extends TestCase {
	 protected ValidarSequencia sequencia = new ValidarSequencia();;

	 public void testvalidarSequencia() {

	 assertEquals("1","Falso",sequencia.validarSequencia("XX"));

	 assertEquals("2","Falso",sequencia.validarSequencia("XSX"));

	 assertEquals("3","Falso",sequencia.validarSequencia("SSXSXSXXXS"));

	 assertEquals("4","Falso",sequencia.validarSequencia("SXSXSXX"));

	 assertEquals("5","Falso",sequencia.validarSequencia("SXSXSXXS"));

	 assertEquals("6","Verdadeiro",sequencia.validarSequencia("S"));

	 assertEquals("7","Verdadeiro",sequencia.validarSequencia("SS"));

	 assertEquals("8","Verdadeiro",sequencia.validarSequencia("SSS"));

	 assertEquals("9","Verdadeiro",sequencia.validarSequencia("SXSX"));

	 assertEquals("10","Verdadeiro",sequencia.validarSequencia("SXSSXX"));

	 assertEquals("11","Verdadeiro",sequencia.validarSequencia("SXSXSSSXSXX"));

	 assertEquals("12","Verdadeiro",sequencia.validarSequencia("SSSSXXXX"));

	 }
}
